/**
 * Gulp file to process minimal Gitlab CI dashboard.
 */

'use strict'

const {src, dest, watch, series, parallel} = require('gulp')
const del = require('del')
const cleanCSS = require('gulp-clean-css')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const sass = require('gulp-sass')(require('sass'))
const terser = require('gulp-terser')
const autoprefixer = require('gulp-autoprefixer')
const htmlReplace = require('gulp-html-replace')
const htmlmin = require('gulp-htmlmin')
const mustache = require('gulp-mustache')
const mergeJson = require('gulp-merge-json')

const srcFolder = './src/'
const distFolder = './public/'
const reportsFolder = './src/reports/'

function clean() {
  return del([
    distFolder + 'js',
    distFolder + 'css',
    distFolder + 'index.html',
    distFolder + 'reports'
  ])
}

function cssBuild(){
  return src(srcFolder + 'sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest(distFolder + 'css'))
}

function cssMinify() {
  return src(distFolder + 'css/*.css')
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(concat('styles.css'))
    .pipe(rename('styles.min.css'))
    .pipe(dest(distFolder + 'css'))
}

function jsVendor() {
  return src([
      'node_modules/materialize-css/js/cash.js',
      'node_modules/materialize-css/js/component.js',
      'node_modules/materialize-css/js/global.js',
      'node_modules/materialize-css/js/anime.min.js',
      'node_modules/materialize-css/js/collapsible.js',
      'node_modules/materialize-css/js/dropdown.js',
      'node_modules/materialize-css/js/waves.js',
      'node_modules/materialize-css/js/sidenav.js',
      'node_modules/materialize-css/js/cards.js',
      'node_modules/materialize-css/js/buttons.js'
    ])
    .pipe(concat('vendor.js'))
    .pipe(dest(distFolder + 'js'))
}

function jsBuild() {
  return src(srcFolder + 'js/*.js')
    .pipe(dest(distFolder + 'js'))
}

function jsMinify() {
  return src([
      distFolder + 'js/vendor.js',
      distFolder + 'js/main.js',
    ])
    .pipe(concat('app.js'))
    .pipe(terser())
    .pipe(rename('app.min.js'))
    .pipe(dest(distFolder + 'js'))
}

function prepareData() {
  return src(srcFolder + 'data/*.json')
    .pipe(mergeJson())
    .pipe(dest(srcFolder + 'data/'));
}

function html() {
  return src(srcFolder + 'templates/index.html')
    .pipe(mustache(srcFolder + 'data/combined.json'))
    .pipe(rename('index.html'))
    .pipe(dest(distFolder))
}

function htmlMinify() {
  return src(distFolder + 'index.html')
    .pipe(htmlReplace({
      css: 'css/styles.min.css',
      js: 'js/app.min.js'
    }))
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeOptionalTags: true,
      minifyJS: true,
      processScripts: ['text/template', 'text/javascript']
    }))
    .pipe(dest(distFolder))
}

function reportsStatic() {
  return src([
      reportsFolder + '**',
      reportsFolder + '**/.css/*',
      reportsFolder + '**/.js/*',
      reportsFolder + '**/.icons/*',
      '!' + reportsFolder + '**/*.less',
      '!' + reportsFolder + '**/*.map'
    ])
    .pipe(dest(distFolder + 'reports'))
}

function reportsCssMinify() {
  return src(reportsFolder + 'report-*/**/*.css')
    .pipe(cleanCSS())
    .pipe(dest(distFolder + 'reports'))
}

function reportsJsMinify() {
  return src(reportsFolder + 'report-*/**/*.js')
    .pipe(terser())
    .pipe(dest(distFolder + 'reports'))
}

exports.watch = function() {
  watch(srcFolder + 'sass/*.scss', cssBuild)
  watch(srcFolder + 'js/*.js', jsBuild)
  watch(srcFolder + 'templates/*', html)
  watch(srcFolder + 'data/*.json', html)
  watch(srcFolder + 'reports/*', reportsStatic)
}

exports.buildDev = series(
  clean,
  parallel(
    cssBuild,
    series(jsVendor, jsBuild),
    series(prepareData, html),
  ),
  reportsStatic
)

exports.build = series(
  clean,
  parallel(
    series(cssBuild, cssMinify),
    series(jsVendor, jsBuild, jsMinify),
    series(prepareData, html, htmlMinify),
  ),
  reportsStatic,
  parallel(
    reportsCssMinify,
    reportsJsMinify
  )
)