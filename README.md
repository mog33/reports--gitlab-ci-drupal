# Reports for Gitlab CI for Drupal

## Test

Copy `data.json` in `./src/data` and `reports` in `./src/` from ci artifact.

### Local nodeJs

Dev:

```shell
yarn build-dev
yarn watch
```

Prod:

```shell
yarn build
```

### NodeJs in docker

```shell
docker build -t=test-dashboard .
docker run -it --rm --name test-dashboard test-dashboard /bin/sh
```

Clean

```shell
docker rmi test-dashboard;
```