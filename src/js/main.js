/**
 * Reports minimal js functions.
 */
(function (window, document) {
  'use strict'

  /**
   * jobs statuses.
   */
  const statuses = {
    success: {
      color: 'green-text',
      text: 'done'
    },
    failed: {
      color: 'red-text',
      text: 'report'
    },
    canceled: {
      color: 'orange-text',
      text: 'cancel'
    },
    skipped: {
      color: '',
      text: 'more_horiz'
    }
  }

  /**
   * Instantiate the report.
   */
  const initApp = () => {
    load('#home')
    sidenavInit()
    handleStaticMenu()
    handleMenu()
  }

  /**
   * Put a template value in the main content.
   *
   * @param string id The DOM id to work with.
   */
  const load = (id) => {
    const template = document.querySelector(id)
    const main = document.getElementById('main')
    if (main) {
      main.innerHTML = template.innerHTML
      show('#main')
    }
  }

  /**
   * Prepare and init the home content.
   *
   * @param string id The DOM id to work with.
   */
  const show = (id) => {
    const content = document.querySelector(id)
    if (content && content.classList.contains('hide')) {
      content.classList.remove('hide')
    }
  }

  /**
   * Handle static pages links.
   *
   * These link will fill the content with a template on the page based on id
   * and empty the iframe of the report.
   */
  const handleStaticMenu = () => {
    const menus = document.querySelectorAll('a.static');
    [].forEach.call(menus, (menu) => {
      menu.addEventListener('click', () => {
        document.querySelector('#info').classList.add('hide')
        cleanActive()
        menu.classList.add('active', 'grey', 'lighten-2')
        const templateId = menu.getAttribute('href')
        load(templateId)
        setReport('')
      })
    })
  }

  /**
   * Fill iframe with source.
   *
   * @param string src The file to display as src.
   */
  const setReport = (src) => {
    const iframe = document.querySelector('#report')
    iframe.setAttribute('src', src)
  }

  /**
   * Prepare and init the sidenav toggle button.
   */
  const sidenavInit = () => {
    const sidenav = document.querySelector('#sidenav')
    M.Sidenav.init(sidenav)

    const toggleSidenav = document.querySelector('#toggle_sidenav')

    toggleSidenav.addEventListener('click', function () {
      // Help to resize body to 100%.
      document.querySelector('body').classList.toggle('no-sidebar')

      // Keep sidenav open on load.
      const sidenav = document.querySelector('#sidenav')
      // eslint-disable-next-line no-undef
      const sidenavInstance = M.Sidenav.getInstance(sidenav)
      if (sidenavInstance.isOpen) {
        sidenavInstance.close()
      } else {
        sidenavInstance.open()
      }
    })
  }

  /**
   * Filter menu by existing jobs and set status.
   *
   * @todo clean sub jobs if not on TOOLS_QA or TOOLS_METRICS / unit-kernel if
   * no coverage.
   */
  const handleMenu = () => {
    const menus = document.querySelectorAll('a.job');

    [].forEach.call(menus, (menu) => {
      // Filter the menu list with status.
      setMenuStatus(menu)
      // Add menu click actions to populate iframe.
      bindMenu(menu)
    })
  }

  /**
   * Remove any class in menu for active element.
   */
  const cleanActive = () => {
    // Clean other active.
    [].forEach.call(document.querySelectorAll('#sidenav .active'), (activeElement) => {
      activeElement.removeAttribute('class')
    })
  }

  /**
   * Initiate and populate menu.
   *
   * @param {*} menu The link element to act on.
   */
  const setMenuStatus = (menu) => {
    const jobId = menu.getAttribute('data-job')
    // eslint-disable-next-line no-undef
    const jobs = dataJobs || {}
    // eslint-disable-next-line no-undef
    const dirs = dataDirs || []

    // Filter existing jobs.
    if (jobs[jobId] !== undefined && jobId !== null) {
      var jobStatus = statuses[jobs[jobId].status]
      var jobMarkup = menu.querySelector('.job-status')

      // If we have a dirs, mean we have file for this job.
      if (dirs.includes('report-' + jobId) !== false) {
        jobMarkup.classList.add(jobStatus.color)
        jobMarkup.innerHTML = jobStatus.text
      }

    } else {
      // If we have a dirs, mean we have file for this job.
      if (dirs.includes('report-' + jobId) !== false) {
        var jobMarkup = menu.querySelector('.job-status')
        jobMarkup.innerHTML = ''
      } else {
        menu.parentNode.remove()
      }
    }
  }

  /**
   * Bind menu links actions.
   *
   * @param {*} menu The link element to act on.
   */
  const bindMenu = (menu) => {
    menu.addEventListener(
      'click',
      () => {
        cleanActive()
        showJobInfo(menu.getAttribute('data-job'))
        // Hide no job page.
        document.querySelector('#main').setAttribute('class', 'hide');
        // Set menu active with color.
        menu.setAttribute('class', 'active blue lighten-2')
        // Populate the iframe with report.
        const report = menu.getAttribute('data-report')
        fetch(report)
          .then((response) => {
            if (response.ok) {
              setReport(report)
            } else {
              setReport('')
              load('#missing')
            }
          })
          .catch((error) => {
            console.log(error.message + '');
          })
      },
      false
    )
  }

  /**
   * Display job information above.
   *
   * @param {*} job The job to work with.
   */
  const showJobInfo = (job) => {
    show('#info')
    const infos = document.querySelectorAll("#info .job")
    infos.forEach((info) => {
      info.classList.add('hide')
    })

    const jobInfo = document.querySelector("#info .job-" + job)
    jobInfo.classList.toggle('hide')
  }

  /**
   * Initiate app.
   */
  document.addEventListener('DOMContentLoaded', () => {
    initApp()
  })

})(this, this.document)